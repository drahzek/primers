package pl.drahzek.primers.model;

import pl.drahzek.primers.PrimeMain;

public class PrimeWorker implements Runnable{
    private long number;

    public PrimeWorker(long number) {
        this.number = number;
    }

    @Override
    public void run() {
        boolean prime = PrimeMain.isPrime(number);
        System.out.println(number + " " + prime);
    }
}
