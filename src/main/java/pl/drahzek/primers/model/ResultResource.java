package pl.drahzek.primers.model;

import pl.drahzek.primers.PrimeMain;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class ResultResource implements Runnable{
    String File;

    public ResultResource(String file) {
        File = file;
    }

    private synchronized void save(String line) {
        try (Writer fw = new FileWriter(File)) {
                fw.write(line);
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    @Override
    public void run() {
        save("saved");
    }
}
