package pl.drahzek.primers;

import pl.drahzek.primers.model.PrimeWorker;
import pl.drahzek.primers.model.ResultResource;
public class PrimeMain {

    public static void main(String[] args) {
//        System.out.println("Give a number");
//        Scanner scanner = new Scanner(System.in);
//        long number = scanner.nextInt();
//        System.out.println(isPrime(number));

        PrimeWorker worker1 = new PrimeWorker(179426629);
        PrimeWorker worker2 = new PrimeWorker(3);
        PrimeWorker worker3 = new PrimeWorker(114);
        PrimeWorker worker4 = new PrimeWorker(2132132132);
        PrimeWorker worker5 = new PrimeWorker(179428003);
        ResultResource resource = new ResultResource("Textwo.txt");

        new Thread(worker1).start();
        new Thread(worker2).start();
        new Thread(worker3).start();
        new Thread(worker4).start();
        new Thread(worker5).start();
        new Thread(resource).start();

    }

    public static boolean isPrime(long number) {
        for (long i = 2; i <= number / 2; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }
}
